/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.volley;

import android.os.Process;
import android.os.SystemClock;
import android.text.TextUtils;

import com.example.vishakhatyagi.volleydemo.Utils;
import com.example.vishakhatyagi.volleydemo.application.VolleyDemoApplication;
import com.example.vishakhatyagi.volleydemo.helpers.Constants;
import com.example.vishakhatyagi.volleydemo.helpers.Enums;
import com.example.vishakhatyagi.volleydemo.models.BusinessObject;

import java.util.concurrent.BlockingQueue;

/**
 * Provides a thread for performing cache triage on a queue of requests.
 *
 * Requests added to the specified cache queue are resolved from cache.
 * Any deliverable response is posted back to the caller via a
 * {@link ResponseDelivery}.  Cache misses and responses that require
 * refresh are enqueued on the specified network queue for processing
 * by a {@link NetworkDispatcher}.
 */
public class CacheDispatcher extends Thread {

    public static final boolean IS_HASHKEY_ENABLED = true;

    private static final boolean DEBUG = VolleyLog.DEBUG;
    private static final String TAG = CacheDispatcher.class.getName();

    /** The queue of requests coming in for triage. */
    private final BlockingQueue<Request<?>> mCacheQueue;

    /** The queue of requests going out to the network. */
    private final BlockingQueue<Request<?>> mNetworkQueue;

    /** The cache to read from. */
    private final Cache mCache;

    /** For posting responses. */
    private final ResponseDelivery mDelivery;

    /** Used for telling us to die. */
    private volatile boolean mQuit = false;

    private VolleyDemoApplication mAppState;

    boolean isUpdCache = false;

    /**
     * Creates a new cache triage dispatcher thread.  You must call {@link #start()}
     * in order to begin processing.
     *
     * @param cacheQueue Queue of incoming requests for triage
     * @param networkQueue Queue to post requests that require network to
     * @param cache Cache interface to use for resolution
     * @param delivery Delivery interface to use for posting responses
     */
    public CacheDispatcher(
            BlockingQueue<Request<?>> cacheQueue, BlockingQueue<Request<?>> networkQueue,
            Cache cache, ResponseDelivery delivery) {
        mCacheQueue = cacheQueue;
        mNetworkQueue = networkQueue;
        mCache = cache;
        mDelivery = delivery;
        mAppState = VolleyDemoApplication.getInstance();
    }

    /**
     * Forces this dispatcher to quit immediately.  If any requests are still in
     * the queue, they are not guaranteed to be processed.
     */
    public void quit() {
        mQuit = true;
        interrupt();
    }

    @Override
    public void run() {
        if (DEBUG) VolleyLog.v("start new dispatcher");
        Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);

        // Make a blocking call to initialize the cache.
        mCache.initialize();

        while (true) {
            try {
                // Get a request from the cache triage queue, blocking until
                // at least one is available.
                final Request<?> request = mCacheQueue.take();
                request.addMarker("cache-queue-take");

                // If the request has been canceled, don't bother dispatching it.
                if (request.isCanceled()) {
                    request.finish("cache-discard-canceled");
                    continue;
                }

                // Attempt to retrieve this item from cache.
                Cache.Entry entry = mCache.get(request.getCacheKey());

                if (entry == null) {
                    if (!request.isCacheOnly()) {
                        request.addMarker("cache-miss");
                        // Cache miss; send off to the network dispatcher.
                        mNetworkQueue.put(request);
                    } else {
                        VolleyError volleyError = new VolleyError(new Throwable("Image download request disabled"));
                        volleyError.setNetworkTimeMs(SystemClock.elapsedRealtime());
                        mDelivery.postError(request, volleyError);
                    }
                    continue;
                }

                if(!TextUtils.isEmpty(request.getUpdTime()) && !TextUtils.isEmpty(entry.upd)) {
                    if(!request.getUpdTime().equalsIgnoreCase(entry.upd)) {

                        request.addMarker("cache-hit-expired");
                        request.setCacheEntry(entry);
                        mNetworkQueue.put(request);
                        continue;
                    }
                    isUpdCache = true;
                }
                else if(!request.isCacheOnly() && request.getIsToBeRefreshed()) {
                    if (Utils.hasInternetAccess(mAppState.getApplicationContext())) {
                        request.addMarker("cache-hit-expired");
                        request.setCacheEntry(entry);
                        mNetworkQueue.put(request);
                        continue;
                    }
                }
                else if(!request.isCacheOnly() && (isCustomCacheExpired(request.getCachingTimeInMins(), entry.lastModified))) {
                    // If it is completely expired, just send it to the network.
                    if(IS_HASHKEY_ENABLED) {
                        if (!request.isHashKeyEnabled()) {
                            if (Utils.hasInternetAccess(mAppState.getApplicationContext())) {
                                request.addMarker("cache-hit-expired");
                                request.setCacheEntry(entry);
                                mNetworkQueue.put(request);
                                continue;
                            }
                        } else {
                            request.addMarker("cache-hit");
                            Response<?> response = request.parseNetworkResponse(
                                    new NetworkResponse(entry.data, entry.responseHeaders));
                            request.addMarker("cache-hit-parsed");

                            if (response.result != null && response.result instanceof BusinessObject)
                                ((BusinessObject) response.result).responseType = Enums.ResponseType.CACHE.ordinal();
                            mDelivery.postResponse(request, response);

                            if (Utils.hasInternetAccess(mAppState.getApplicationContext())) {
                                request.addMarker("cache-hit-expired");
                                request.setCacheEntry(entry);
                                mNetworkQueue.put(request);
                            }
                            continue;
                        }
                    }
                    else {
                        if (Utils.hasInternetAccess(mAppState.getApplicationContext())) {
                            request.addMarker("cache-hit-expired");
                            request.setCacheEntry(entry);
                            mNetworkQueue.put(request);
                            continue;
                        }
                    }
                }

                // We have a cache hit; parse its data for delivery back to the request.
                request.addMarker("cache-hit");
                Response<?> response = request.parseNetworkResponse(
                        new NetworkResponse(entry.data, entry.responseHeaders));
                request.addMarker("cache-hit-parsed");

                if (!entry.refreshNeeded() || request.isCacheOnly() || isUpdCache) {
                    isUpdCache = false;
                    // Completely unexpired cache hit. Just deliver the response.
                    if(Constants.IS_HASHKEY_ENABLED) {
                        if (response.result != null && response.result instanceof BusinessObject)
                            ((BusinessObject) response.result).responseType = Enums.ResponseType.CACHE_NOT_EXPIRED.ordinal();
                    }
                    mDelivery.postResponse(request, response);
                } else {
                    // Soft-expired cache hit. We can deliver the cached response,
                    // but we need to also send the request to the network for
                    // refreshing.
                    request.addMarker("cache-hit-refresh-needed");
                    request.setCacheEntry(entry);

                    // Mark the response as intermediate.
                    response.intermediate = true;

                    // Post the intermediate response back to the user and have
                    // the delivery then forward the request along to the network.
                    mDelivery.postResponse(request, response, new Runnable() {
                        @Override
                        public void run() {
                            try {
                                mNetworkQueue.put(request);
                            } catch (InterruptedException e) {
                                // Not much we can do about this.
                            }
                        }
                    });
                }

            } catch (InterruptedException e) {
                // We may have been interrupted because it was time to quit.
                if (mQuit) {
                    return;
                }
                continue;
            }
        }
    }

    private boolean isCustomCacheExpired(int cachingTimeInMins, long lastModified) {
        if(cachingTimeInMins != 0 && (lastModified + (cachingTimeInMins * 60 * 1000L) < System.currentTimeMillis())) {
            return true;
        }
        return false;
    }
}