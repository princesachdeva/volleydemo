package com.android.volley.toolbox;


import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Response;

/**
 * Created by jyotishman.baruah on 09/01/18.
 */

class CrashlyticsApiLoggingInterceptor implements Interceptor {
    @Override
    public Response intercept(Chain chain) throws IOException {
     //   Crashlytics.log(chain.request().url().toString());
        return chain.proceed(chain.request());
    }
}
