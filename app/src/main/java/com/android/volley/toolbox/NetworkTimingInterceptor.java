package com.android.volley.toolbox;

import android.text.TextUtils;
import android.util.Log;


import com.example.vishakhatyagi.volleydemo.Utils;
import com.example.vishakhatyagi.volleydemo.manager.FeedRequest;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by jyotishman.baruah on 26/12/17.
 */

class NetworkTimingInterceptor implements Interceptor {
    private static final String TAG = NetworkTimingInterceptor.class.getSimpleName();

    @Override
    public Response intercept(Chain chain) throws IOException {

        Request request = chain.request();
        if (TextUtils.isEmpty(request.header(FeedRequest.HEADER_PARAM_X_TRACK)))
            return chain.proceed(request);

        long startNs = System.nanoTime();
        Response response =  chain.proceed(request);

        long tookMs = TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - startNs);

        String category = request.header(FeedRequest.HEADER_PARAM_X_TRACK);
        String sectionName = request.header(FeedRequest.HEADER_PARAM_X_TRACK_SN);

        StringBuilder sb = new StringBuilder("+++++++++++++++++++++++++++++++++");
        sb.append("\n");
        sb.append("API - ").append(request.url());
        sb.append("\n");
        sb.append("CATEGORY - ").append(category);
       // if (RemoteConfigHelper.getInstance().getBooleanValue(RemoteConfigHelper.Keys.MAP_SDK_ENABLED))
            sb.append("_MAP");
        sb.append("\n");
        sb.append("SECTION NAME - ").append(sectionName);
        sb.append("\n");
        sb.append("TIME - ").append(tookMs +"ms");
        sb.append("\n");
        sb.append("NETWORK - ").append(Utils.getNetworkType());
        sb.append("\n");
        sb.append("+++++++++++++++++++++++++++++++++");

        Log.i(TAG, sb.toString());


  //      GoogleAnalyticsManager.getInstance().trackUserTiming(category,tookMs,sectionName, Utils.getNetworkType());

        return response;
    }
}
