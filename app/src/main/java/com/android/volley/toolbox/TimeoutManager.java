package com.android.volley.toolbox;

import android.text.TextUtils;

import com.android.volley.Request;
import com.example.vishakhatyagi.volleydemo.Utils;
import com.example.vishakhatyagi.volleydemo.helpers.UrlConstants;

/**
 * Created by jyotishman.baruah on 09/01/18.
 */

class TimeoutManager {
    private static final int FIFTEEN_SECONDS_MILLIS = 15 * 1000;
    private static final int FIVE_SECONDS_MILLIS = 5 * 1000;

    public static int getTimeoutInMillis(Request<?> request) {
        String url =  request.getUrl();
        if (!TextUtils.isEmpty(url) && url.startsWith(UrlConstants.MASTER_URL)){
           /* if (Utils.isFirstLaunch()){
                return FIFTEEN_SECONDS_MILLIS;
            }else {*/
                return FIVE_SECONDS_MILLIS;
    //        }
        }
        return request.getTimeoutMs();
    }
}
