/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.volley.toolbox;

import android.content.Context;

import com.android.volley.Network;
import com.android.volley.RequestQueue;

import java.io.File;

public class Volley {

    /** Default on-disk cache directory. */
    private static final String DEFAULT_CACHE_DIR = "com/android/volley";

    private static final String DEFAULT_CACHE_DIR_IMG = "com/android/volleyimg";

    private static final int IMAGE_THREAD_POOL_SIZE = 2;
    private static final int GENERIC_THREAD_POOL_SIZE = 3;
    private static final int IMAGE_DEFAULT_DISK_USAGE_BYTES = 10 * 1024 * 1024;
    private static final int GENERIC_DEFAULT_DISK_USAGE_BYTES = 10 * 1024 * 1024;


    /**
     * Creates a default instance of the worker pool and calls {@link RequestQueue#start()} on it.
     *
     * @param context A {@link Context} to use for creating the cache dir.
     * @param stack   An {@link HttpStack} to use for the network, or null for default.
     * @return A started {@link RequestQueue} instance.
     */
    public static RequestQueue newRequestQueue(Context context, HttpStack stack) {
        File cacheDir = new File(context.getCacheDir(), DEFAULT_CACHE_DIR);

        if (stack == null) {
            stack = HttpStackProvider.getHttpStack();
        }

        Network network = new BasicNetwork(stack);

        RequestQueue queue = new RequestQueue(new DiskBasedCache(cacheDir, GENERIC_DEFAULT_DISK_USAGE_BYTES), network, GENERIC_THREAD_POOL_SIZE);
        queue.start();

        return queue;
    }

    public static RequestQueue newImageRequestQueue(Context context, HttpStack stack) {
        File cacheDir = new File(context.getCacheDir(), DEFAULT_CACHE_DIR_IMG);

        if (stack == null) {
            stack = HttpStackProvider.getHttpStack();
        }

        Network network = new BasicNetwork(stack);

        RequestQueue queue = new RequestQueue(new DiskBasedCache(cacheDir, IMAGE_DEFAULT_DISK_USAGE_BYTES), network, IMAGE_THREAD_POOL_SIZE);
        queue.start();

        return queue;
    }

    /**
     * Creates a default instance of the worker pool and calls {@link RequestQueue#start()} on it.
     *
     * @param context A {@link Context} to use for creating the cache dir.
     * @return A started {@link RequestQueue} instance.
     */
    public static RequestQueue newRequestQueue(Context context) {
        return newRequestQueue(context, null);
    }
}
