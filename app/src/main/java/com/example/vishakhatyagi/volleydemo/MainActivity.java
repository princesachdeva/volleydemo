package com.example.vishakhatyagi.volleydemo;

import android.content.Context;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.akamai.android.sdk.VocRegistrationInfo;
import com.akamai.android.sdk.VocService;
import com.example.vishakhatyagi.volleydemo.helpers.Constants;
import com.feed.MasterFeedItems;
import com.feed.RootFeedItems;
import com.feed.RootFeedManager;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {


    private Context mContext;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initializeMapSDK();
        ((Button)findViewById(R.id.btn)).setOnClickListener(this);
    //    initApi();
    }

    private void initializeMapSDK() {
        if (!VocService.createVocService(getApplicationContext()).getRegistrationStatus().isActive()) {
            VocRegistrationInfo regInfo;
            if (BuildConfig.DEBUG) {
                regInfo = new VocRegistrationInfo(Constants.MAP_SDK_DEBUG_KEY);
            } else {
                regInfo = new VocRegistrationInfo(Constants.MAP_SDK_RELEASE_KEY);
            }
            VocService.createVocService(getApplicationContext()).register(regInfo, new VocService.VocAysncResponseHandler(new Handler()) {
                @Override
                public void onSuccess() {

                }

                @Override
                public void onFailure(String s) {

                }
            });
        }
    }

    private void initApi() {
        final RootFeedManager rootFeedManager = RootFeedManager.getInstance();

        rootFeedManager.initMasterFeed(new RootFeedManager.iMasterFeedFetched() {
            @Override
            public void onMasterFeedFetched(MasterFeedItems rootFeedItems) {
                Log.d("TAG", "onMasterFeedFetched");

                rootFeedManager.initRootFeed(new RootFeedManager.iRootFeedFetched() {
                    @Override
                    public void onRootFeedFetched(RootFeedItems rootFeedItems) {
                        Log.d("TAG", "onRootFeedFetched");
                        /*handleSplashAd();
                        Constants.log("onRootFeedFetched rootFeedItems = " + rootFeedItems);*/
                        //load Section for LHS
                        //load Home
                        //if (adFeedItems == null) {
                /*} else {
                    launchHomeView();
                }*/
                    }

                    @Override
                    public void onRootFeedError(int errCode) {

                    }
                }, getApplicationContext());
            }

            @Override
            public void onMasterFeedFetchedFromCache(MasterFeedItems rootFeedItems) {

            }

            @Override
            public void onMasterFeedError(int errCode) {

            }

        }, getApplicationContext());
    }

    @Override
    public void onClick(View v) {
        initApi();
    }
}
