package com.example.vishakhatyagi.volleydemo;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.telephony.TelephonyManager;

import com.example.vishakhatyagi.volleydemo.application.VolleyDemoApplication;
import com.example.vishakhatyagi.volleydemo.helpers.Enums;

/**
 * Created by vishakha.tyagi on 26/02/2018.
 */

public class Utils {

    private static Enums.ConnectionType[] mConnectionType;


    private static SharedPreferences mSettings = null;


    private static ConnectivityManager mCM;

    public static boolean hasInternetAccess(Context context) {
        if (context == null) {
            return false;
        }
        if (mCM == null) {
            mCM = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        }
        NetworkInfo netInfo = mCM.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnected())    // Modified in Rev. 211
        {
            return true;
        }
        return false;
    }

    //Preference Keys
    public static String getStringPreferences(Context ctx, String key) {
        if (mSettings == null) {
            mSettings = ctx.getSharedPreferences(ctx.getResources().getString(R.string.home_settings_file), 0);
        }
        if (mSettings != null) {
            return mSettings.getString(key, "");
        } else return "";
    }

    public static int getVersionCode(Context ctx) {
        int versionCode = 300;
        /*try {
            versionCode = ctx.getPackageManager().getPackageInfo(ctx.getPackageName(), 0).versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }*/
        return versionCode;
    }

    public static String getDensityName(Context context) {
        float density = context.getResources().getDisplayMetrics().density;
        if (density >= 4.0) {
            return "xxxhdpi";
        }
        if (density >= 3.0) {
            return "xxhdpi";
        }
        if (density >= 2.0) {
            return "xhdpi";
        }
        if (density >= 1.5) {
            return "hdpi";
        }
        if (density >= 1.0) {
            return "mdpi";
        }
        return "hdpi";
    }

    public static Enums.ConnectionType[] getConnectionType(Context context) {
        if (mConnectionType == null) {
            resetDownloadImageSettings(context);
        }
        return mConnectionType;
    }

    public static void resetDownloadImageSettings(Context context) {
        mConnectionType = new Enums.ConnectionType[3];
        mConnectionType[1] = Enums.ConnectionType.H_SPEED;
        mConnectionType[1] = Enums.ConnectionType.L_SPEED;
        mConnectionType[2] = Enums.ConnectionType.WIFI;
    }

    public static String getNetworkType() {

        if (!isConnected(VolleyDemoApplication.getInstance().getApplicationContext())) {
            return "";
        }

        if (isInWifi(VolleyDemoApplication.getInstance().getApplicationContext())) {
            return "WIFI";
        }

        TelephonyManager mTelephonyManager = (TelephonyManager) VolleyDemoApplication.getInstance().getApplicationContext()
                .getSystemService(Context.TELEPHONY_SERVICE);
        int networkType = mTelephonyManager.getNetworkType();
        switch (networkType) {
            case TelephonyManager.NETWORK_TYPE_GPRS:
            case TelephonyManager.NETWORK_TYPE_EDGE:
            case TelephonyManager.NETWORK_TYPE_CDMA:
            case TelephonyManager.NETWORK_TYPE_1xRTT:
            case TelephonyManager.NETWORK_TYPE_IDEN:
            case TelephonyManager.NETWORK_TYPE_GSM:
                return "2G";
            case TelephonyManager.NETWORK_TYPE_UMTS:
            case TelephonyManager.NETWORK_TYPE_EVDO_0:
            case TelephonyManager.NETWORK_TYPE_EVDO_A:
            case TelephonyManager.NETWORK_TYPE_HSDPA:
            case TelephonyManager.NETWORK_TYPE_HSUPA:
            case TelephonyManager.NETWORK_TYPE_HSPA:
            case TelephonyManager.NETWORK_TYPE_EVDO_B:
            case TelephonyManager.NETWORK_TYPE_EHRPD:
            case TelephonyManager.NETWORK_TYPE_HSPAP:
            case TelephonyManager.NETWORK_TYPE_TD_SCDMA:

                return "3G";
            case TelephonyManager.NETWORK_TYPE_LTE:
            case TelephonyManager.NETWORK_TYPE_IWLAN:
                return "4G";
            default:
                return "N/A";
        }
    }

    public static boolean isConnected(Context mContext) {
        ConnectivityManager conMan = (ConnectivityManager) mContext
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = conMan.getActiveNetworkInfo();
        return (info != null && info.isConnected());
    }

    public static boolean isInWifi(Context mContext) {
        ConnectivityManager conMan = (ConnectivityManager) mContext
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo.State wifi = conMan.getNetworkInfo(ConnectivityManager.TYPE_WIFI)
                .getState();
        return wifi == NetworkInfo.State.CONNECTED;
    }



}
