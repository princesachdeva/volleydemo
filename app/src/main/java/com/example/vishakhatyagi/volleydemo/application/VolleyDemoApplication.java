package com.example.vishakhatyagi.volleydemo.application;

import android.app.Application;

/**
 * Created by vishakha.tyagi on 26/02/2018.
 */

public class VolleyDemoApplication extends Application {

    private static VolleyDemoApplication mInstance;


    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
    }


    public static VolleyDemoApplication getInstance() {
        if (mInstance == null)
            mInstance = new VolleyDemoApplication();
        return mInstance;
    }

    public boolean isAppInDataSaveMode() {
        return false;
    }
}
