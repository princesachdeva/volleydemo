package com.example.vishakhatyagi.volleydemo.helpers;

/**
 * Created by vishakha.tyagi on 26/02/2018.
 */

public class Constants {

    public static final boolean IS_HASHKEY_ENABLED = true;

    public final static int DEFAULT_CACHING_TIME = 10;

    public static final String MAP_SDK_DEBUG_KEY = "cb1ccbbbb9e77a04e28a3730623fcd302b20468941d989f81a2752627e772738";
    public static final String MAP_SDK_RELEASE_KEY = "4ce6f1a360e9b54b7babe4f6f5a9a8f23533663100aabeb67324fb75aeab9d20";
}
