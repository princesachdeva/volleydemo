package com.example.vishakhatyagi.volleydemo.helpers;

/**
 * Created by vivek.varun on 12/14/16.
 */

public class Enums {
    public static enum ConnectionType {WIFI, H_SPEED, L_SPEED}

    public static enum ResponseType { NETWORK, CACHE, NOT_MODIFIED, CACHE_NOT_EXPIRED }
}
