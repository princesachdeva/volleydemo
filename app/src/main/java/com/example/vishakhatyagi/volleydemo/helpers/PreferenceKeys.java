package com.example.vishakhatyagi.volleydemo.helpers;

/**
 * Created by vishakha.tyagi on 26/02/2018.
 */

public class PreferenceKeys {

    //RootFeedItems
    public static final String MASTER_FEED_ITEMS = "master_feed_items";
    public static final String ROOT_FEED_ITEMS = "root_feed_items";
}
