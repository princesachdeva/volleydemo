package com.example.vishakhatyagi.volleydemo.helpers;

/**
 * Created by vishakha.tyagi on 26/02/2018.
 */

public class UrlConstants {

    public final static String BASE_URL = "http://economictimes.indiatimes.com/";

    public final static String SCHEME_HTTP = "http";
    public final static String SCHEME_HTTPS = "https://";

    public final static String MASTER_URL = BASE_URL + "masterfeed_app.cms?feedtype=etjson";
    ;

    public final static String PARAMETER_RES = "res";
    public static final String ANDROID_VERSION_PARAMETER = "&andver=";
}
