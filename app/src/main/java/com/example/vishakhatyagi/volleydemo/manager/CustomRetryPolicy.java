package com.example.vishakhatyagi.volleydemo.manager;

import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;

public class CustomRetryPolicy implements RetryPolicy {
    @Override
    public int getCurrentTimeout() {
        return 20000;
    }

    @Override
    public int getCurrentRetryCount() {
        return 1;
    }

    @Override
    public void retry(VolleyError error) throws VolleyError {

    }
}
