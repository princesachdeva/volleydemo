package com.example.vishakhatyagi.volleydemo.manager;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.widget.ImageView;

import com.android.volley.Cache;
import com.android.volley.ParseError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.example.vishakhatyagi.volleydemo.Utils;
import com.example.vishakhatyagi.volleydemo.application.VolleyDemoApplication;
import com.example.vishakhatyagi.volleydemo.helpers.Enums;
import com.example.vishakhatyagi.volleydemo.models.BusinessObject;
import com.example.vishakhatyagi.volleydemo.volley.CustomImageLoader;
import com.example.vishakhatyagi.volleydemo.volley.MultipartFeedRequest;
import com.example.vishakhatyagi.volleydemo.volley.VolleyUtils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.util.ConnectionUtil;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Modifier;
import java.util.List;

/**
 * Created by vishakha.tyagi on 26/02/2018.
 */

public class FeedManager {
    private static final String TAG = "FeedManager";
    private static FeedManager mFeedManager;

    public interface OnImageLoadedListener {
        public void onImageLoaded(Bitmap bitmap);
    }

    public static FeedManager getInstance() {
        if (mFeedManager == null) {
            synchronized (FeedManager.class) {
                if (mFeedManager == null)
                    mFeedManager = new FeedManager();
            }
        }
        return mFeedManager;
    }

    public void queueJob(final FeedParams feedParams) {
        if (!feedParams.isCacheOnly()) {
            FeedRequest jsonRequest = new FeedRequest(feedParams.getMethod(), feedParams.getUrl(), feedParams.getClassName(), feedParams.listener, feedParams.errorListener);
            jsonRequest.setShouldCache(feedParams.shouldCache());
            jsonRequest.setRetrying(feedParams.isRetrying());
            jsonRequest.setCachingTimeInMins(feedParams.getCachingTimeInMins());
            jsonRequest.setUpdTime(feedParams.getUpdTime());
            jsonRequest.setCacheKey(feedParams.getUrl());
            jsonRequest.isToBeRefreshed(feedParams.getIsToBeRefreshed());
            jsonRequest.setTag(feedParams.getTag());
            jsonRequest.setPriority(feedParams.getPriority());
            jsonRequest.setHashKeyEnabled(feedParams.isHashKeyEnabled());
            jsonRequest.setTitle(feedParams.getTitle());
            jsonRequest.setParseType(feedParams.getParseType());
            jsonRequest.setTrackingCategory(feedParams.getTrackingCategory());
            jsonRequest.setTrackingSectionName(feedParams.getTrackingSectionName());
            VolleyUtils.getInstance().addToRequestQueue(jsonRequest);
        } else {
            getDataFromCache(feedParams);
        }
    }

    public void getDataFromCache(FeedParams feedParams) {
        Cache cache = VolleyUtils.getInstance().getRequestQueue().getCache();
        Cache.Entry entry = cache.get(feedParams.getUrl());
        if (entry != null) {
            try {
                String data = new String(entry.data, "UTF-8");
                // handle data, like converting it to xml, json, bitmap etc.,
                try {
                    if (feedParams.getClassName() != null && feedParams.getClassName() != String.class) {
                        Gson gson = new GsonBuilder().excludeFieldsWithModifiers(Modifier.STATIC, Modifier.PROTECTED).create();
                        feedParams.listener.onResponse((BusinessObject) gson.fromJson(data, feedParams.getClassName()));
                    }
                } catch (Exception e) {
                    feedParams.errorListener.onErrorResponse((new ParseError(e)));
                }

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        feedParams.listener.onResponse(null);
    }

    public void clearCache() {
        Cache cache = VolleyUtils.getInstance().getRequestQueue().getCache();
        if (cache != null) {
            if (cache.getKeysList() != null) {
                List<String> keys = cache.getKeysList();
                for (String key : keys) {
                    Cache.Entry entry = cache.get(key);
                    if (entry != null) {
                        if (entry.isExpired()) {
                            cache.remove(key);
                        }
                    }
                }
            }
            cache.clear();
        }
    }

    public void bindImage(final ImageView imageView, String url) {
        ImageLoader imageLoader = VolleyUtils.getInstance().getImageLoader();
        imageLoader.get(url, new ImageLoader.ImageListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG, "Image Load Error: " + error.getMessage());
                    }

                    @Override
                    public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                        if (response.getBitmap() != null) {
                            // load image into imageview
                            imageView.setImageBitmap(response.getBitmap());
                        }
                    }
                }
        );
    }

    public void bindImage(final ImageView imageView, String url, final OnImageLoadedListener listener) {
        ImageLoader imageLoader = VolleyUtils.getInstance().getImageLoader2();
        imageLoader.get(url, new ImageLoader.ImageListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG, "Image Load Error: " + error.getMessage());
                    }

                    @Override
                    public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                        if (response.getBitmap() != null) {
                            // load image into imageview
                            if (listener != null)
                                listener.onImageLoaded(response.getBitmap());
                            imageView.setImageBitmap(response.getBitmap());
                        }
                    }
                }
        );
    }

    public void queueJobMultipart(final FeedParams feedParams, String body) {
        if (!feedParams.isCacheOnly()) {
            MultipartFeedRequest jsonRequest = new MultipartFeedRequest(feedParams.getMethod(), feedParams.getUrl(), feedParams.getClassName(), feedParams.listener, feedParams.errorListener);
            jsonRequest.setBody(body);
            jsonRequest.setShouldCache(feedParams.shouldCache());
            jsonRequest.setTag(feedParams.getTag());
            jsonRequest.setPriority(feedParams.getPriority());
            jsonRequest.setParseType(feedParams.getParseType());
            VolleyUtils.getInstance().addToRequestQueue(jsonRequest);
        } else {
            getDataFromCache(feedParams);
        }
    }

   /* public void bindImage(NetworkImageView imgNetWorkView, String url) {
        ImageLoader imageLoader = VolleyUtils.getInstance().getImageLoader();
        imgNetWorkView.setTag(R.id.volley_image_url_tag_id, url);
        imgNetWorkView.setImageUrl(url, imageLoader);
    }

    //call this method only when we require to maintain no cache
    public void bindImageFromNetwork(NetworkImageView imgNetWorkView, String url, int maxWidth, int maxHeight, ImageView.ScaleType scaleType) {
        ImageLoader imageLoader = VolleyUtils.getInstance().getImageLoader();
        imgNetWorkView.setTag(R.id.volley_image_url_tag_id, url);
        //clear LRU Cache
        ((CustomImageLoader) imageLoader).clearMemoryCache(url, maxWidth, maxHeight, scaleType);
        //clear Disk Cache
        VolleyUtils.getInstance().getImageRequestQueue().getCache().invalidate(url, true);
        VolleyUtils.getInstance().getImageRequestQueue().getCache().remove(url);
        imgNetWorkView.setImageUrl(url, imageLoader);
    }*/

    public void getBitmap(String url, final Interfaces.OnBitmapRetrieved onBitmapRetrieved) {
        getBitmap(url, onBitmapRetrieved, true, false);
    }

    public void getBitmap(String url, final Interfaces.OnBitmapRetrieved onBitmapRetrieved, boolean shouldConsiderCacheFirst) {
        getBitmap(url, onBitmapRetrieved, shouldConsiderCacheFirst, false);
    }

    public void getLargeBitmap(String url, final Interfaces.OnBitmapRetrieved onBitmapRetrieved) {
        Enums.ConnectionType[] conTypesAllowed = Utils.getConnectionType(VolleyDemoApplication.getInstance());
        Enums.ConnectionType userConType = ConnectionUtil.getConnectionType(VolleyDemoApplication.getInstance());
        boolean isCacheOnly = true;
        for (Enums.ConnectionType conType : conTypesAllowed) {
            if (conType == userConType) {
                isCacheOnly = false;
                break;
            }
        }

        getBitmap(url, onBitmapRetrieved, true, isCacheOnly);
    }

    public void getBitmap(String url, final Interfaces.OnBitmapRetrieved onBitmapRetrieved, boolean shouldConsiderCacheFirst, boolean isCacheOnly) {
        ImageLoader imageLoader = VolleyUtils.getInstance().getImageLoader();
        final Bitmap bitmap;

        isCacheOnly = (isCacheOnly || VolleyDemoApplication.getInstance().isAppInDataSaveMode());
        imageLoader.get(url, new ImageLoader.ImageListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG, "Image Load Error: " + error.getMessage());
                        if (onBitmapRetrieved != null)
                            onBitmapRetrieved.onErrorResponse(error);
                    }

                    @Override
                    public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                        if (response.getBitmap() != null) {
                            // load image into imageview
                            if (onBitmapRetrieved != null)
                                onBitmapRetrieved.onSuccessfulResponse(response.getBitmap());
                        }
                    }
                }
                , 0, 0, ImageView.ScaleType.CENTER_INSIDE, isCacheOnly, shouldConsiderCacheFirst);
    }

    /**
     * Will update existing cache.So bitmapManager will fetch image directly from cache.
     *
     * @param url
     * @param bitmap
     */
    public void updateImageCache(String url, Bitmap bitmap) {
        //cache.put(url, new SoftReference<Bitmap>(bitmap));
        ImageLoader imageLoader = VolleyUtils.getInstance().getImageLoader();
        //clear LRU Cache
        if (bitmap != null && ((CustomImageLoader) imageLoader).getBitmapFromCache(url) == null) {
            ((CustomImageLoader) imageLoader).updateMemoryCache(url, bitmap);
        }
    }

    public static Bitmap getBitmapFromDisk(String url) {
        try {
            if (!new File(url).exists())
                return null;

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.RGB_565;
            Bitmap bitmap = BitmapFactory.decodeFile(url, options);
            if (bitmap == null)
                Log.w(TAG, "Fetching failed from Disc.Url is " + url);
            return bitmap;
        } catch (OutOfMemoryError error) {//TODO:Not a very good idea but for some devices its throwing for high resolution(Tested for 768*576)
            return null;
        } catch (Exception ex) {
            Log.w(TAG, "EXCEPTION:Error : " + ex.getMessage());
            return null;
        }
    }

    public Bitmap getBitmapFromCache(String url) {
        ImageLoader imageLoader = VolleyUtils.getInstance().getImageLoader();
        return ((CustomImageLoader) imageLoader).getBitmapFromCache(url);
    }

    public class BitmapResponse {
        private Bitmap bmp;

        private Bitmap getBmp() {
            return bmp;
        }

        private void setBmp(Bitmap bmp) {
            this.bmp = bmp;
        }
    }
}
