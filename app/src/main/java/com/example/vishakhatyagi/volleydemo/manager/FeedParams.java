package com.example.vishakhatyagi.volleydemo.manager;

import com.android.volley.Request;
import com.android.volley.Response;
import com.example.vishakhatyagi.volleydemo.helpers.Constants;

import java.util.Map;

public class FeedParams {
    private int method = Request.Method.GET;
    private Class<?> className;
    private String url;
    private String tag;
    private String title;



    private boolean isCacheOnly;
    private boolean isToClearCookies;
    private boolean isToBeRefreshed;
    private int cachingTimeInMins = Constants.DEFAULT_CACHING_TIME;
    private boolean shouldCache = true;
    private Map<String, String> postParams;
    private Map<String, String> headerParams;
    private Request.Priority priority = Request.Priority.NORMAL;
    private FeedRequest.PARSE_TYPE parseType = FeedRequest.PARSE_TYPE.JSON;

    private boolean isHashKeyEnabled = false;

    public Response.Listener<Object> listener;
    public Response.ErrorListener errorListener;
    public Response.ResponseNotModifiedListener responseNotModifiedListener;

    public String updTime;

    private String trackingCategory;
    private String trackingSectionName;

    public FeedParams(String url,
                      Class<?> className,
                      Response.Listener<Object> listener,
                      Response.ErrorListener errorListener){
        this(url,className,listener,errorListener,null);
    }

    public FeedParams(String url,
                      Class<?> className,
                      Response.Listener<Object> listener,
                      Response.ErrorListener errorListener,
                      Response.ResponseNotModifiedListener responseNotModifiedListener){
        this.url = url;
        this.className = className;
        this.listener = listener;
        this.errorListener = errorListener;
        this.responseNotModifiedListener = responseNotModifiedListener;
    }

    public String getTrackingCategory() {
        return trackingCategory;
    }

    public void setTrackingCategory(String trackingCategory) {
        this.trackingCategory = trackingCategory;
    }

    public int getMethod() {
        return method;
    }

    public void setMethod(int method) {
        this.method = method;
    }

    public Class<?> getClassName() {
        return className;
    }

    public String getUrl() {
        return url;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getUpdTime() {
        return updTime;
    }

    public void setUpdTime(String updTime) {
        this.updTime = updTime;
    }

    public boolean isCacheOnly() {
        return isCacheOnly;
    }

    public void setIsCacheOnly(boolean isCacheOnly) {
        this.isCacheOnly = isCacheOnly;
    }

    public void setCachingTimeInMins(int cachingTimeInMins) {
        this.cachingTimeInMins = cachingTimeInMins;
    }

    public int getCachingTimeInMins() {
        return this.cachingTimeInMins;
    }

    public Map<String, String> getPostParams() {
        return postParams;
    }

    public void setPostParams(Map<String, String> postParams) {
        this.postParams = postParams;
    }

    public Map<String, String> getHeaderParams() {
        return headerParams;
    }

    public void setHeaderParams(Map<String, String> headerParams) {
        this.headerParams = headerParams;
    }

    public boolean shouldCache() {
        return shouldCache;
    }

    public void setShouldCache(boolean shouldCache) {
        this.shouldCache = shouldCache;
    }

    public void isToBeRefreshed(boolean isToBeRefreshed) {
        this.isToBeRefreshed = isToBeRefreshed;
    }

    public boolean getIsToBeRefreshed() {
        return isToBeRefreshed;
    }

    public void isToClearCookies(boolean isToClearCookies) {
        this.isToClearCookies = isToClearCookies;
    }

    public Request.Priority getPriority() {
        return priority;
    }

    public void setPriority(Request.Priority priority) {
        this.priority = priority;
    }
    public FeedRequest.PARSE_TYPE getParseType() {
        return parseType;
    }

    public void setParseType(FeedRequest.PARSE_TYPE parseType) {
        this.parseType = parseType;
    }

    public boolean isRetrying() {
        return retrying;
    }

    public void setRetrying(boolean retrying) {
        this.retrying = retrying;
    }

    private boolean retrying = false;

    public void setTitle(String title){
        this.title = title;
    }

    public String getTitle(){
        return title;
    }

    public boolean isHashKeyEnabled() {
        return isHashKeyEnabled;
    }

    public void setHashKeyEnabled(boolean isHashKeyEnabled) {
        this.isHashKeyEnabled = isHashKeyEnabled;
    }

    public void setTrackingSectionName(String trackingSectionName) {
        this.trackingSectionName = trackingSectionName;
    }

    public String getTrackingSectionName() {
        return trackingSectionName;
    }
}
