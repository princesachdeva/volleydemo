package com.example.vishakhatyagi.volleydemo.manager;

import android.text.TextUtils;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.example.vishakhatyagi.volleydemo.Utils;
import com.example.vishakhatyagi.volleydemo.application.VolleyDemoApplication;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.Map;

public class FeedRequest extends Request<Object> {
    public static final String HEADER_PARAM_X_TRACK    = "X-TRACK";
    public static final String HEADER_PARAM_X_TRACK_SN    = "X-TRACK-SN";

    private static Map<String, String> headerParams = new HashMap<String, String>();
    private Map<String, String> trackingHeaders = new HashMap<String, String>();

    private Class<?> className;
    private Response.Listener<Object> listener;
    private Response.Listener<Object> responseNotModified;
    private Priority priority = Priority.NORMAL;
    private PARSE_TYPE parse_type = PARSE_TYPE.JSON;
    private String mCacheKey;

    public void setTrackingCategory(String trackingCategory) {
        trackingHeaders.put(HEADER_PARAM_X_TRACK,trackingCategory);
    }

    public void setTrackingSectionName(String trackingSectionName) {
        trackingHeaders.put(HEADER_PARAM_X_TRACK_SN,trackingSectionName);
    }

    public enum PARSE_TYPE {JSON, JSOUP, STRING, RSS}

    private boolean retrying = false;
    static String __VIEWSTATE;
    static String __CALLBACKID = "__Page";
    private String title;
    private static final String[] NON_JSON_CHAR = {"var etmarketcompanydata=", "var etmarketdata=", "var mcxlivedata=", "calenderspread(", "premiumdiscount(", "var otherCommodityData=", "marketlivedata("};

    public boolean isRetrying() {
        return retrying;
    }

    public void setRetrying(boolean retrying) {
        this.retrying = retrying;
    }

    public FeedRequest(int method, String url, Class<?> className, Response.Listener<Object> listener, Response.ErrorListener errorListener) {
        super(method, url, errorListener);
        this.className = className;
        this.listener = listener;
    }

    public FeedRequest(int method, String url, Class<?> className, Response.Listener<Object> listener, Response.ErrorListener errorListener, Response.Listener<Object> responseNotModified) {
        super(method, url, errorListener);
        this.className = className;
        this.listener = listener;
        this.responseNotModified = responseNotModified;
    }

    public void setHeaderParams(Map<String, String> headerParams) {
        this.headerParams = headerParams;
    }

    @Override
    public RetryPolicy getRetryPolicy() {
        return new CustomRetryPolicy();
    }

    @Override
    protected Response<Object> parseNetworkResponse(NetworkResponse response) {
        try {
            String json = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
            json = updateResponseToJson(json);
            if (!TextUtils.isEmpty(json)) {
                json = json.trim();
                if(!json.startsWith("{")) {
                    json = "{data:" + json + "}";
                }
            }
            if (className != null && className != String.class) {
                Gson gson = new GsonBuilder()
                        .excludeFieldsWithModifiers(Modifier.STATIC, Modifier.PROTECTED)
                        .registerTypeAdapterFactory(new ArrayAdapterFactory())
                        .create();
                return Response.success(gson.fromJson(json, className), HttpHeaderParser.parseCacheHeaders(response));
            } else
                return Response.success((Object) json, HttpHeaderParser.parseCacheHeaders(response));

        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
            return Response.error(new ParseError(e));
        }
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        trackingHeaders.putAll(headerParams);
        return trackingHeaders;
    }

    @Override
    public void deliverError(VolleyError error) {
        super.deliverError(error);
        if (!Utils.hasInternetAccess(VolleyDemoApplication.getInstance().getApplicationContext())) {
            //Toast.makeText(ETApplication.getInstance().getApplicationContext(), "Not connected to Internet", Toast.LENGTH_SHORT).show();
        }
    }

    public void setParseType(PARSE_TYPE parseType) {
        this.parse_type = parseType;
    }

    @Override
    protected void deliverResponse(Object response) {
        listener.onResponse(response);
    }

    @Override
    public void deliverResponseNotModified(Object response) {
        super.deliverResponseNotModified(response);
    }

    @Override
    public Priority getPriority() {
        return priority;
    }

    public void setPriority(Priority priority) {
        this.priority = priority;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    @Override
    public String getCacheKey() {
        return mCacheKey;
    }

    public void setCacheKey(String cacheKey) {
        this.mCacheKey = cacheKey;
    }

    private String updateResponseToJson(String json) {
        if (!TextUtils.isEmpty(json)) {
            json = json.trim();
            for (String string : NON_JSON_CHAR) {
                json = json.replace(string, "");
            }

            if (json.endsWith(")")) {
                json = json.replace(")", "");
            }
        }
        return json;
    }
}
