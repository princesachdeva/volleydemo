package com.example.vishakhatyagi.volleydemo.manager;

import android.graphics.Bitmap;

import com.android.volley.VolleyError;

/**
 * Created by vishakha.tyagi on 26/02/2018.
 */

public class Interfaces {

    public interface OnBitmapRetrieved {
        public void onSuccessfulResponse(Bitmap bitmap);

        public void onErrorResponse(VolleyError volleyError);
    }
}
