package com.example.vishakhatyagi.volleydemo.models;

import com.android.volley.VolleyError;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by vishakha.tyagi on 26/02/2018.
 */

public class BusinessObject implements Serializable {
    private static final long serialVersionUID = 1L;

    private VolleyError volleyError;

    public int responseType;

    @SerializedName("updl")
    private String updl;

    public String getId() {
        return null;
    }

    public ArrayList<?> getArrlistItem() {
        return null;
    }

    public boolean shouldRetry() {
        return shouldRetry;
    }

    public void setShouldRetry(boolean shouldRetry) {
        this.shouldRetry = shouldRetry;
    }

    private boolean shouldRetry = false;

    private String lastRefreshTime = "";

    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public VolleyError getVolleyError() {
        return volleyError;
    }

    public void setVolleyError(VolleyError volleyError) {
        this.volleyError = volleyError;
    }

    public String getLastRefreshTime() {
        return lastRefreshTime;
    }

    public void setLastRefreshTime(String lastRefreshTime) {
        this.lastRefreshTime = lastRefreshTime;
    }

    public String getEpochTime() {
        return null;
    }

    public String getHashKey() {
        return updl;
    }
}

