package com.example.vishakhatyagi.volleydemo.volley;

import android.graphics.Bitmap;
import android.widget.ImageView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;

public class CustomImageLoader extends ImageLoader {

    ImageCache mCache;
    String mCacheKey;
    /**
     * Constructs a new ImageLoader.
     *
     * @param queue      The RequestQueue to use for making image requests.
     * @param imageCache The cache to use as an L1 cache.
     */
    public CustomImageLoader(RequestQueue queue, ImageCache imageCache) {
        super(queue, imageCache);
        mCache = imageCache;
    }


    public void clearMemoryCache(String url, int maxWidth, int maxHeight, ImageView.ScaleType scaleType){
        String cacheKey = getCacheKey(url, maxWidth, maxHeight, scaleType);
        ((LruBitmapCache)mCache).remove(cacheKey);
    }

    public void updateMemoryCache(String url, Bitmap bitmap){
        updateMemoryCache(url, bitmap, 0, 0, ImageView.ScaleType.CENTER_INSIDE);

    }

    public void updateMemoryCache(String url, Bitmap bitmap, int maxWidth, int maxHeight, ImageView.ScaleType scaleType){
        String cacheKey = getCacheKey(url, maxWidth, maxHeight, scaleType);
        mCache.putBitmap(cacheKey, bitmap);
    }

    public Bitmap getBitmapFromCache(String url) {
        return getBitmapFromCache(url, 0, 0, ImageView.ScaleType.CENTER_INSIDE);
    }

    public Bitmap getBitmapFromCache(String url, int maxWidth, int maxHeight, ImageView.ScaleType scaleType){
        String cacheKey = getCacheKey(url, maxWidth, maxHeight, scaleType);
        Bitmap bitmap = mCache.getBitmap(cacheKey);
        return bitmap;
    }

    private static String getCacheKey(String url, int maxWidth, int maxHeight, ImageView.ScaleType scaleType) {
        return new StringBuilder(url.length() + 12).append("#W").append(maxWidth)
                .append("#H").append(maxHeight).append("#S").append(scaleType.ordinal()).append(url)
                .toString();
    }
}
