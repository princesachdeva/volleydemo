package com.example.vishakhatyagi.volleydemo.volley;

import android.text.TextUtils;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.example.vishakhatyagi.volleydemo.manager.FeedRequest;

import java.net.URLEncoder;

public class MultipartFeedRequest extends FeedRequest {
    private String body;
    private String encodingType = "application/x-www-form-urlencoded; charset=UTF-8";

    static String __VIEWSTATE;
    static String __CALLBACKID = "__Page";

    public MultipartFeedRequest(int method, String url, Class<?> className, Response.Listener<Object> listener, Response.ErrorListener errorListener) {
        super(method, url, className, listener, errorListener);
    }

    public void setBody(String body){
        if(!TextUtils.isEmpty(__VIEWSTATE))
            body = body +"&__VIEWSTATE="+ URLEncoder.encode(__VIEWSTATE)+"&__CALLBACKID="+URLEncoder.encode(__CALLBACKID);
        Log.d("Test", "body is {"+body+"}");
        body = body.replaceAll(" ","");
        this.body = body;
    }

    public void setContentType(String encodingType){
        this.encodingType = encodingType;
    }

    @Override
    public byte[] getBody() throws AuthFailureError {
        return body.getBytes();
    }

    @Override
    public String getBodyContentType() {
        return encodingType;
    }
}
