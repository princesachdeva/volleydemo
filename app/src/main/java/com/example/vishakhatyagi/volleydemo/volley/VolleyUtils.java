package com.example.vishakhatyagi.volleydemo.volley;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.HttpStackProvider;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.example.vishakhatyagi.volleydemo.application.VolleyDemoApplication;
import com.example.vishakhatyagi.volleydemo.manager.FeedRequest;

import java.util.HashMap;
import java.util.Map;

public class VolleyUtils {
    private static VolleyUtils instance;
    private RequestQueue mRequestQueue;
    private RequestQueue mImageRequestQueue;
    private ImageLoader mImageLoader, mImageLoader2;
    private LruBitmapCache mLruBitmapCache = null;

    public static VolleyUtils getInstance() {
        if (instance == null) {
            instance = new VolleyUtils();
        }
        return instance;
    }

    private VolleyUtils() {
        init();
    }

    public void init() {
    }

    public LruBitmapCache getmLruBitmapCache() {
        return mLruBitmapCache;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(VolleyDemoApplication.getInstance(), HttpStackProvider.getHttpStack());
        }

        return mRequestQueue;
    }

    public boolean isFromCache() {
        if (mLruBitmapCache != null) {
            return mLruBitmapCache.isFromCache();
        } else {
            return false;
        }
    }

    public RequestQueue getImageRequestQueue() {
        if (mImageRequestQueue == null) {
            mImageRequestQueue = Volley.newImageRequestQueue(VolleyDemoApplication.getInstance(), HttpStackProvider.getHttpStack());
        }

        return mImageRequestQueue;
    }

    public ImageLoader getImageLoader() {
        if (mImageLoader == null) {
            if (mLruBitmapCache == null) {
                mLruBitmapCache = new LruBitmapCache();
            }
            mImageLoader = new CustomImageLoader(getImageRequestQueue(), mLruBitmapCache);
        }
        return this.mImageLoader;
    }

    public ImageLoader getImageLoader2() {
        if (mLruBitmapCache == null) {
            mLruBitmapCache = new LruBitmapCache();
        }
        if (mImageLoader2 == null) {
            mImageLoader2 = new ImageLoader(getRequestQueue(), mLruBitmapCache);
        }
        return this.mImageLoader2;
    }

    public <T> void addToRequestQueue(Request<T> req) {
        if (req.getPriority() == Request.Priority.HIGH) {
            //We need to call getHighPriorityRequestQueue() once we will handle all the aspects
            getRequestQueue().add(req);
        } else {
            getRequestQueue().add(req);
        }
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }

    public void cancelPendingImageRequests(Object tag) {
        if (mImageRequestQueue != null) {
            mImageRequestQueue.cancelAll(tag);
        }
    }

    public void resetCache() {
        if (mRequestQueue != null) {
            mRequestQueue.getCache().clear();
        }

        if (mImageRequestQueue != null) {
            mImageRequestQueue.getCache().clear();
        }
    }

    public void removeCache(String url) {
        if (mRequestQueue != null) {
            mRequestQueue.getCache().remove(url);
        }
    }

    public void invalidateCache(String url) {
        if (mRequestQueue != null) {
            mRequestQueue.getCache().invalidate(url, true);
        }
    }

    public Map<String, String> getHeaderParams(FeedRequest feedRequest) {
        Map<String, String> headerParams = new HashMap<String, String>();
        headerParams.put("Accept-Encoding", "gzip");
        return headerParams;
    }
}