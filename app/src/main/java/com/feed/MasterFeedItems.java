package com.feed;


import android.text.TextUtils;

import com.example.vishakhatyagi.volleydemo.helpers.UrlConstants;
import com.example.vishakhatyagi.volleydemo.models.BusinessObject;
import com.google.gson.annotations.SerializedName;

public class MasterFeedItems extends BusinessObject {
    private static final long serialVersionUID = 1L;

    @SerializedName("root")
    private String root;

    @SerializedName("root_upd")
    private String root_upd;

    public String getRoot() {
        if (!TextUtils.isEmpty(root) && !root.startsWith(UrlConstants.SCHEME_HTTP))
            root = UrlConstants.BASE_URL + root;
        return root;
    }

    public String getRoot_upd() {
        return root_upd;
    }
}
