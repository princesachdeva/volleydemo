package com.feed;

import android.text.TextUtils;

import com.example.vishakhatyagi.volleydemo.helpers.UrlConstants;
import com.example.vishakhatyagi.volleydemo.models.BusinessObject;
import com.google.gson.annotations.SerializedName;

public class RootFeedItems extends BusinessObject {
    @SerializedName("upd")
    private String upd;
    @SerializedName("chkf")
    private String chkf;
    @SerializedName("chk_upd")
    private String chk_upd;
    @SerializedName("adf")
    private String adf;
    @SerializedName("adf_upd")
    private String adf_upd;
    @SerializedName("lhs")
    private String lhs;
    @SerializedName("lhs_upd")
    private String lhs_upd;
    @SerializedName("tabs")
    private String tabs;
    @SerializedName("tabs_upd")
    private String tabs_upd;
    @SerializedName("home")
    private String home;
    @SerializedName("stryf")
    private String stryf;
    @SerializedName("slidef")
    private String slidef;
    @SerializedName("videof")
    private String videof;
    @SerializedName("topicf")
    private String topicf;
    @SerializedName("hubUrl")
    private String hubUrl;
    @SerializedName("app_ver")
    private String app_ver;
    @SerializedName("ccl")
    private String ccl;
    @SerializedName("pcu")
    private String pcu;
    @SerializedName("ccu")
    private String ccu;
    @SerializedName("qrdpl")
    private String qrdpl;
    @SerializedName("bnews")
    private String bnews;
    @SerializedName("arcu")
    private String arcu;
    @SerializedName("eb")
    private String eb;
    @SerializedName("mb")
    private String mb;
    @SerializedName("searchf")
    private String searchf;
    @SerializedName("audiof")
    private String audiof;
    @SerializedName("sharef")
    private String sharef;
    @SerializedName("nb")
    private String nb;
    @SerializedName("pr_st")
    private String pr_st;
    @SerializedName("pre_text")
    private String pre_text;
    @SerializedName("subga")
    private String subga;
    @SerializedName("sync_t")
    private String sync_t;
    @SerializedName("sync_c")
    private String sync_c;
    private int numSessionBeforePermissionsRequest;

    public int getNumSessionBeforePermissionsRequest() {
        return numSessionBeforePermissionsRequest;
    }

    public void setNumSessionBeforePermissionsRequest(int numSessionBeforePermissionsRequest) {
        this.numSessionBeforePermissionsRequest = numSessionBeforePermissionsRequest;
    }

    @SerializedName("splashad")
    private SplashAd splashAd;

    public String getNb() {
        if (!TextUtils.isEmpty(nb) && !nb.startsWith(UrlConstants.SCHEME_HTTP))
            nb = UrlConstants.BASE_URL + nb;
        return nb;
    }

    public String getShareFeed() {
        if (!TextUtils.isEmpty(sharef) && !sharef.startsWith(UrlConstants.SCHEME_HTTP))
            sharef = UrlConstants.BASE_URL + sharef;
        return sharef;
    }

    public String getCheckFeed() {
        if (!TextUtils.isEmpty(chkf) && !chkf.startsWith(UrlConstants.SCHEME_HTTP))
            chkf = UrlConstants.BASE_URL + chkf;
        return chkf;
    }

    public String getAdFeed() {
        if (!TextUtils.isEmpty(adf) && !adf.startsWith(UrlConstants.SCHEME_HTTP))
            adf = UrlConstants.BASE_URL + adf;
        return adf;
    }

    public String getLhsFeed() {
        if (!TextUtils.isEmpty(lhs) && !lhs.startsWith(UrlConstants.SCHEME_HTTP))
            lhs = UrlConstants.BASE_URL + lhs;
        return lhs;
    }

    public String getTabsFeed() {
        if (!TextUtils.isEmpty(tabs) && !tabs.startsWith(UrlConstants.SCHEME_HTTP))
            tabs = UrlConstants.BASE_URL + tabs;
        return tabs;
    }

    public String getHomeFeed() {
        if (!TextUtils.isEmpty(home) && !home.startsWith(UrlConstants.SCHEME_HTTP))
            home = UrlConstants.BASE_URL + home;
        return home;
    }

    public String getStoryFeed() {
        if (!TextUtils.isEmpty(stryf) && !stryf.startsWith(UrlConstants.SCHEME_HTTP))
            stryf = UrlConstants.BASE_URL + stryf;
        return stryf;
    }

    public String getSlideshowFeed() {
        if (!TextUtils.isEmpty(slidef) && !slidef.startsWith(UrlConstants.SCHEME_HTTP))
            slidef = UrlConstants.BASE_URL + slidef;
        return slidef;
    }

    public String getVideoFeed() {
        if (!TextUtils.isEmpty(videof) && !videof.startsWith(UrlConstants.SCHEME_HTTP))
            videof = UrlConstants.BASE_URL + videof;
        return videof;
    }

    public String getTopicFeed() {
        if (!TextUtils.isEmpty(topicf) && !topicf.startsWith(UrlConstants.SCHEME_HTTP))
            topicf = UrlConstants.BASE_URL + topicf;
        return topicf;
    }

    public String getNotificationHubUrl() {
        if (!TextUtils.isEmpty(hubUrl) && !hubUrl.startsWith(UrlConstants.SCHEME_HTTP))
            hubUrl = UrlConstants.BASE_URL + hubUrl;
        return hubUrl;
    }

    public String getCommentListUrl() {
        if (!TextUtils.isEmpty(ccl) && !ccl.startsWith(UrlConstants.SCHEME_HTTP))
            ccl = UrlConstants.BASE_URL + ccl;
        return ccl;
    }

    public String getCommentPostUrl() {
        if (!TextUtils.isEmpty(pcu) && !pcu.startsWith(UrlConstants.SCHEME_HTTP))
            pcu = UrlConstants.BASE_URL + pcu;
        return pcu;
    }

    public String getAppVer() {
        if (!TextUtils.isEmpty(app_ver) && !app_ver.startsWith(UrlConstants.SCHEME_HTTP))
            app_ver = UrlConstants.BASE_URL + app_ver;
        return app_ver;
    }

    public String getCommentsCountUrl() {
        if (!TextUtils.isEmpty(ccu) && !ccu.startsWith(UrlConstants.SCHEME_HTTP))
            ccu = UrlConstants.BASE_URL + ccu;
        return ccu;
    }

    public String getQuickReadUrl() {
        if (!TextUtils.isEmpty(qrdpl) && !qrdpl.startsWith(UrlConstants.SCHEME_HTTP))
            qrdpl = UrlConstants.BASE_URL + qrdpl;
        return qrdpl;
    }

    public String getBreakingNewsUrl() {
        if (!TextUtils.isEmpty(bnews) && !bnews.startsWith(UrlConstants.SCHEME_HTTP))
            bnews = UrlConstants.BASE_URL + bnews;
        return bnews;
    }

    public String getDailyBriefArcu() {
        if (!TextUtils.isEmpty(arcu) && !arcu.startsWith(UrlConstants.SCHEME_HTTP))
            arcu = UrlConstants.BASE_URL + arcu;
        return arcu;
    }

    public String getMorningBriefUrl() {
        if (!TextUtils.isEmpty(mb) && !mb.startsWith(UrlConstants.SCHEME_HTTP))
            mb = UrlConstants.BASE_URL + mb;
        return mb;
    }

    public String getEveningBriefUrl() {
        if (!TextUtils.isEmpty(eb) && !eb.startsWith(UrlConstants.SCHEME_HTTP))
            eb = UrlConstants.BASE_URL + eb;
        return eb;
    }

    public String getSearchFeed() {
        if (!TextUtils.isEmpty(searchf) && !searchf.startsWith(UrlConstants.SCHEME_HTTP))
            searchf = UrlConstants.BASE_URL + searchf;
        return searchf;
    }

    public String getCheckEpochTime() {
        return chk_upd;
    }

    public String getAdfEpochTime() {
        return adf_upd;
    }

    public String getLhsEpochTime() {
        return lhs_upd;
    }

    public String getTabsEpochTime() {
        return tabs_upd;
    }

    public String getAudioFeed() {
        return audiof;
    }

    public String getPremiumStatement() {
        return pr_st;
    }

    public String getPremiumText() {
        return pre_text;
    }

    public String getSubga() {
        return subga;
    }

    public String getSubscriptionSyncText() {
        return sync_t;
    }

    public String getSubscriptionSyncSuccessText() {
        return sync_c;
    }

    public SplashAd getSplashAd() {
        return splashAd;
    }

    @Override
    public String getEpochTime() {
        return upd;
    }

    public class SplashAd extends BusinessObject {
        @SerializedName("showsplash")
        private String showSplash;

        @SerializedName("time_sec")
        private String timeSec;

        @SerializedName("had")
        private String headerAd;

        @SerializedName("fad")
        private String footerAd;

        @SerializedName("intad")
        private String intAd;

        @SerializedName("upd")
        private String upd;

        @SerializedName("hadlp")
        private String headerAdClick;

        @SerializedName("fadlp")
        private String footerAdClick;

        @SerializedName("intadlp")
        private String intAdClick;

        public String getShowSplash() {
            return showSplash;
        }

        public String getTimeSec() {
            return timeSec;
        }

        public String getHeaderAdUrl() {
            if (!TextUtils.isEmpty(headerAd) && !headerAd.startsWith(UrlConstants.SCHEME_HTTP))
                headerAd = UrlConstants.BASE_URL + headerAd;
            return headerAd;
        }

        public String getFooterAdUrl() {
            if (!TextUtils.isEmpty(footerAd) && !footerAd.startsWith(UrlConstants.SCHEME_HTTP))
                footerAd = UrlConstants.BASE_URL + footerAd;
            return footerAd;
        }

        public String getIntAdUrl() {
            if (!TextUtils.isEmpty(intAd) && !intAd.startsWith(UrlConstants.SCHEME_HTTP))
                intAd = UrlConstants.BASE_URL + intAd;
            return intAd;
        }

        public String getUpd() {
            return upd;
        }

        public String getHeaderAdClick() {
            return headerAdClick;
        }

        public String getFooterAdClick() {
            return footerAdClick;
        }

        public String getIntAdClick() {
            return intAdClick;
        }
    }
}