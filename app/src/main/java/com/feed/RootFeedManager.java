package com.feed;

import android.content.Context;
import android.telephony.SubscriptionManager;
import android.text.TextUtils;
import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.example.vishakhatyagi.volleydemo.Utils;
import com.example.vishakhatyagi.volleydemo.application.VolleyDemoApplication;
import com.example.vishakhatyagi.volleydemo.helpers.PreferenceKeys;
import com.example.vishakhatyagi.volleydemo.helpers.UrlConstants;
import com.example.vishakhatyagi.volleydemo.manager.FeedManager;
import com.example.vishakhatyagi.volleydemo.manager.FeedParams;
import com.util.Serializer;

/**
 * Created by vishakha.tyagi on 26/02/2018.
 */

public class RootFeedManager {

    private MasterFeedItems masterFeedItems = null;
    private RootFeedItems mRootFeedItems = null;


    public interface iMasterFeedFetched {
        void onMasterFeedFetched(MasterFeedItems rootFeedItems);

        void onMasterFeedFetchedFromCache(MasterFeedItems rootFeedItems);

        void onMasterFeedError(int errCode);
    }

    public interface iRootFeedFetched {
        void onRootFeedFetched(RootFeedItems rootFeedItems);

        void onRootFeedError(int errCode);
    }

    private static RootFeedManager mInstance = null;

    public static RootFeedManager getInstance() {
        if (mInstance == null) {
            synchronized (RootFeedManager.class) {
                if (null == mInstance)
                    mInstance = new RootFeedManager();
            }
        }
        return mInstance;
    }


    public void initRootFeed(final iRootFeedFetched listener, Context context) {
        MasterFeedItems masterFeedItems = getMasterFeedItems();
        if (masterFeedItems == null || TextUtils.isEmpty(masterFeedItems.getRoot())) {
            initMasterFeed(new iMasterFeedFetched() {
                @Override
                public void onMasterFeedFetched(MasterFeedItems rootFeedItems) {
                    fetchRootFeed(listener, rootFeedItems);
                }

                @Override
                public void onMasterFeedError(int errCode) {
                    listener.onRootFeedError(ErrorCodes.FEED_ERROR);
                }
                @Override
                public void onMasterFeedFetchedFromCache(MasterFeedItems rootFeedItems) {
                    fetchRootFeed(listener, rootFeedItems);
                }
            }, context);
            return;
        }
        fetchRootFeed(listener, masterFeedItems);
    }


    private void fetchRootFeed(final iRootFeedFetched listener, MasterFeedItems masterFeedItems) {
        FeedParams feedParams = new FeedParams(masterFeedItems.getRoot(), RootFeedItems.class,
                new Response.Listener<Object>() {
                    @Override
                    public void onResponse(Object response) {
                        //Log.d("RootFeed", "Api hit done");
                        RootFeedItems rootFeedItems = null;
                        if (response != null && response instanceof RootFeedItems) {
                            rootFeedItems = ((RootFeedItems) response);
                            mRootFeedItems = rootFeedItems;
                       //     VolleyDemoApplication.getInstance().setRootFeedItems(rootFeedItems);
                         //   persistRootFeedItems(rootFeedItems);
                        }
                        if (null != rootFeedItems)
                            listener.onRootFeedFetched(rootFeedItems);
                        else
                            listener.onRootFeedError(ErrorCodes.NETWORK_ERROR);
                    }
                }
                , new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                listener.onRootFeedError(ErrorCodes.FEED_ERROR);
            }
        });
        feedParams.setTrackingCategory("Root Feed");
        feedParams.setTrackingSectionName("Root Feed");
        feedParams.isToBeRefreshed(false);
        feedParams.setUpdTime(masterFeedItems.getRoot_upd());
        FeedManager.getInstance().queueJob(feedParams);
    }

    public MasterFeedItems getMasterFeedItems() {
        if (masterFeedItems == null) {
            masterFeedItems = (MasterFeedItems) Serializer.deserialize(Utils.getStringPreferences(VolleyDemoApplication.getInstance(), PreferenceKeys.MASTER_FEED_ITEMS));
        }
        return masterFeedItems;
    }

    public RootFeedItems getRootFeedItems() {
        if (mRootFeedItems == null) {
            mRootFeedItems = (RootFeedItems) Serializer.deserialize(Utils.getStringPreferences(VolleyDemoApplication.getInstance(), PreferenceKeys.ROOT_FEED_ITEMS));
        }
        return mRootFeedItems;
    }


    public void initMasterFeed(final iMasterFeedFetched listener, Context context) {
        String masterUrl = UrlConstants.MASTER_URL;
        String versionCode = String.valueOf(Utils.getVersionCode(context));
        if (!TextUtils.isEmpty(masterUrl)) {
            if (masterUrl.contains("?"))
                masterUrl = masterUrl + "&" + UrlConstants.PARAMETER_RES + "=" + Utils.getDensityName(context);
            else
                masterUrl = masterUrl + "?" + UrlConstants.PARAMETER_RES + "=" + Utils.getDensityName(context);
        }
        if (!TextUtils.isEmpty(versionCode))
            masterUrl = masterUrl + UrlConstants.ANDROID_VERSION_PARAMETER + versionCode;

       /* if (RemoteConfigHelper.getInstance().getBooleanValue(RemoteConfigHelper.Keys.HAPTIK_ENABLED))
            masterUrl = masterUrl + UrlConstants.HAPTIK_PARAMETER + "true";
*/
       /* SubscriptionManager subscriptionManager = SubscriptionManager.getInstance();
        if (!TextUtils.isEmpty(subscriptionManager.status))
            masterUrl = masterUrl + UrlConstants.STATUS + subscriptionManager.status;
        if (!TextUtils.isEmpty(subscriptionManager.planType)) {
            masterUrl = masterUrl + UrlConstants.PLAN_TYPE + subscriptionManager.planType;
        }
        if (!TextUtils.isEmpty(subscriptionManager.error))
            masterUrl = masterUrl + UrlConstants.ERROR + subscriptionManager.error;

        masterUrl = masterUrl + UrlConstants.PLATFORM + UrlConstants.PLATFORM_VALUE; */
        FeedParams feedParams = new FeedParams(masterUrl, MasterFeedItems.class,
                new Response.Listener<Object>() {
                    @Override
                    public void onResponse(Object response) {
                        Log.d("RootFeed", "Api hit done");
                        MasterFeedItems masterFeedItems = null;

                        if (response != null && response instanceof MasterFeedItems) {

                            masterFeedItems = ((MasterFeedItems) response);
                            RootFeedManager.this.masterFeedItems = masterFeedItems;
                      //      persistMasterFeedItems(masterFeedItems);
                        }
                        if (null != masterFeedItems)
                            listener.onMasterFeedFetched(masterFeedItems);
                      //  else
                      //      handleMasterFeedError(listener, ErrorCodes.NETWORK_ERROR);
                    }
                }
                , new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("haptik", "error is-->" + error.getMessage());
           //     handleMasterFeedError(listener, ErrorCodes.FEED_ERROR);

            }
        });
        feedParams.setTrackingCategory("Master Feed");
        feedParams.setTrackingSectionName("Master Feed");
        feedParams.setShouldCache(false);
        feedParams.isToBeRefreshed(true);
        FeedManager.getInstance().queueJob(feedParams);
    }

}
