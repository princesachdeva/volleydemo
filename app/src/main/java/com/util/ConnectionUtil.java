package com.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.telephony.TelephonyManager;

import com.example.vishakhatyagi.volleydemo.helpers.Enums;


/**
 * Created by vivek.varun on 12/14/16.
 */

public class ConnectionUtil {

    /**
     * Check if there is any connectivity
     *
     * @param context
     * @return
     */
    public static boolean isConnected(Context context) {
        NetworkInfo info = ConnectionUtil.getNetworkInfo(context);
        return (info != null && info.isConnected());
    }

    /**
     * Will return null in case of no internet connection
     *
     * @param context
     * @return
     */
    public static Enums.ConnectionType getConnectionType(Context context) {
        if (!isConnected(context)) {
            return null;
        }
        int conType = getNetworkInfo(context).getType();
        int conSubType = getNetworkInfo(context).getSubtype();
        if (conType == ConnectivityManager.TYPE_WIFI) {
            return Enums.ConnectionType.WIFI;
        } else if (conType == ConnectivityManager.TYPE_MOBILE) {
            switch (conSubType) {
                case TelephonyManager.NETWORK_TYPE_1xRTT:// ~ 50-100 kbps
                case TelephonyManager.NETWORK_TYPE_CDMA:// ~ 14-64 kbps
                case TelephonyManager.NETWORK_TYPE_EDGE:// ~ 50-100 kbps
                case TelephonyManager.NETWORK_TYPE_IDEN: // // ~25 kbps API level 8
                case TelephonyManager.NETWORK_TYPE_GPRS:// ~ 100 kbps
                    return Enums.ConnectionType.L_SPEED;

                case TelephonyManager.NETWORK_TYPE_EVDO_0:// ~ 400-1000 kbps
                case TelephonyManager.NETWORK_TYPE_EVDO_A: // ~ 600-1400 kbps
                case TelephonyManager.NETWORK_TYPE_HSDPA:// ~ 2-14 Mbps
                case TelephonyManager.NETWORK_TYPE_HSPA: // ~ 700-1700 kbps
                case TelephonyManager.NETWORK_TYPE_HSUPA:// ~ 1-23 Mbps
                case TelephonyManager.NETWORK_TYPE_UMTS:// ~ 400-7000 kbps
                /*
                 * Above API level 7, make sure to set android:targetSdkVersion
				 * to appropriate level to use these
				 */
                case TelephonyManager.NETWORK_TYPE_EHRPD: // API level 11 ~ 1-2 Mbps
                case TelephonyManager.NETWORK_TYPE_EVDO_B: // API level 9 ~ 5 Mbps
                case TelephonyManager.NETWORK_TYPE_HSPAP: // API level 13~ 10-20 Mbps
                case TelephonyManager.NETWORK_TYPE_LTE: // API level 11 ~ 10+ Mbps
                    return Enums.ConnectionType.H_SPEED;
                // Unknown
                case TelephonyManager.NETWORK_TYPE_UNKNOWN:
                default:
                    return Enums.ConnectionType.H_SPEED; //Intentionally 3g so that every thing works fine
            }
        }
        return null;
    }

    /**
     * Get the network info
     *
     * @param context
     * @return
     */
    private static NetworkInfo getNetworkInfo(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo();
    }

}
